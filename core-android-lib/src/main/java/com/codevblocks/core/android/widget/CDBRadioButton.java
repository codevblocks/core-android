package com.codevblocks.core.android.widget;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatRadioButton;

public class CDBRadioButton extends AppCompatRadioButton implements CDBWidget {

    public CDBRadioButton(final Context context) {
        this(context, null, 0);
    }

    public CDBRadioButton(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CDBRadioButton(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
