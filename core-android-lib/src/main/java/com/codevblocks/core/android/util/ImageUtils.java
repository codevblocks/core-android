package com.codevblocks.core.android.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.FileDescriptor;
import java.io.InputStream;

public final class ImageUtils {

    private ImageUtils() {}

    public static final Rect getSize(@NonNull final InputStream inputStream) {
        return getSize(inputStream, null);
    }

    public static final Rect getSize(@NonNull final InputStream inputStream, @Nullable final BitmapFactory.Options options) {
        final BitmapFactory.Options decodeOptions = options != null ? options : new BitmapFactory.Options();
        decodeOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeStream(inputStream, null, decodeOptions);

        return new Rect(0, 0, decodeOptions.outWidth, decodeOptions.outHeight);
    }

    public static final Rect getSize(@NonNull final FileDescriptor fileDescriptor) {
        final BitmapFactory.Options decodeOptions = new BitmapFactory.Options();
        decodeOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeFileDescriptor(fileDescriptor, null, decodeOptions);

        return new Rect(0, 0, decodeOptions.outWidth, decodeOptions.outHeight);
    }

    public static final int getSampleSize(final int width, final int height, final int targetWidth, final int targetHeight) {
        int sampleSize = 1;

        int sampleWidth = width;
        int sampleHeight = height;

        while ((sampleWidth /= 2) >= targetWidth && (sampleHeight /= 2) >= targetHeight) {
            sampleSize *= 2;
        }

        return sampleSize;
    }

    public static final Bitmap decode(@NonNull final FileDescriptor fileDescriptor, final int targetWidth, final int targetHeight) {
        return decode(fileDescriptor, targetWidth, targetHeight, null);
    }

    public static final Bitmap decode(@NonNull final FileDescriptor fileDescriptor, final int targetWidth, final int targetHeight, @Nullable final BitmapFactory.Options options) {
        final BitmapFactory.Options decodeOptions = options != null ? options : new BitmapFactory.Options();

        final Rect imageSize = getSize(fileDescriptor);
        final int imageSampleSize = getSampleSize(imageSize.width(), imageSize.height(), targetWidth, targetHeight);

        decodeOptions.inJustDecodeBounds = false;
        decodeOptions.inSampleSize = imageSampleSize;

        return BitmapFactory.decodeFileDescriptor(fileDescriptor, null, decodeOptions);
    }

}
