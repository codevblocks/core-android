package com.codevblocks.core.android.widget;

import com.codevblocks.core.android.R;

interface CDBWidget {

    int[] STATE_ERROR = new int[] { R.attr.state_error };
    int[] STATE_PASSWORD_MASKED = new int[] { R.attr.state_password_masked };

    int ERROR_TEXT_VIEW_MODE_ALWAYS = 0;
    int ERROR_TEXT_VIEW_MODE_FOCUSED = 1;

}
