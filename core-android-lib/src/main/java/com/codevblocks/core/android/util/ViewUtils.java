package com.codevblocks.core.android.util;

import android.view.View;
import android.view.ViewParent;

public final class ViewUtils {

    private ViewUtils() {}

    public static final int[] getViewPositionInParent(final View view, final ViewParent parent, final int[] outPosition) {
        int[] position = outPosition != null ? outPosition : new int[2];

        if (view.getParent() == parent) {
            position[0] = view.getLeft();
            position[1] = view.getTop();
        } else {
            position = getViewPositionInParent( (View) view.getParent(), parent, position );
            position[0] = position[0] + view.getLeft();
            position[1] = position[1] + view.getTop();
        }

        return position;
    }

    public static final View getParentWithId(final View view, final int parentId) {
        ViewParent viewParent = view.getParent();
        View parentView;

        while (viewParent != null) {
            parentView = (View) viewParent;

            if (parentView.getId() == parentId) {
                return parentView;
            }

            viewParent = viewParent.getParent();
        }

        return null;
    }

    public static final View getTopLevelParent(final View view) {
        View topLevelParent = view;

        ViewParent viewParent;
        while ((viewParent = topLevelParent.getParent()) != null && viewParent instanceof View) {
            topLevelParent = (View) viewParent;
        }

        return topLevelParent;
    }

}
