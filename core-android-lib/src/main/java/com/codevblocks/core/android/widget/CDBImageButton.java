package com.codevblocks.core.android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageButton;

import com.codevblocks.core.android.R;

public class CDBImageButton extends AppCompatImageButton implements CDBWidget {

    private static final float DEFAULT_ASPECT_RATIO = 0F;

    private float mAspectRatio;

    public CDBImageButton(final Context context) {
        super(context);

        initialize(context, null);
    }

    public CDBImageButton(final Context context, final AttributeSet attrs) {
        super(context, attrs);

        initialize(context, attrs);
    }

    public CDBImageButton(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initialize(context, attrs);
    }

    private final void initialize(final Context context, final AttributeSet attrs) {
        this.mAspectRatio = DEFAULT_ASPECT_RATIO;

        final TypedArray typedArray = attrs != null ? context.getTheme().obtainStyledAttributes(attrs, R.styleable.CDBImageButton, 0, 0) : null;

        if (attrs != null) {
            try {
                setAspectRatio(typedArray.getFloat(R.styleable.CDBImageButton_aspectRatio, mAspectRatio));
            } finally {
                typedArray.recycle();
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        final int measuredWidth = getMeasuredWidth();
        final int measuredHeight = getMeasuredHeight();

        if (Float.compare(0F, mAspectRatio) != 0) {
            final int widthMeasureSpecMode = MeasureSpec.getMode(widthMeasureSpec);
            final int widthMeasureSpecSize = MeasureSpec.getSize(widthMeasureSpec);
            final int heightMeasureSpecMode = MeasureSpec.getMode(heightMeasureSpec);
            final int heightMeasureSpecSize = MeasureSpec.getSize(heightMeasureSpec);

            int width = measuredWidth;
            int height = measuredHeight;

            if (widthMeasureSpecMode == MeasureSpec.EXACTLY) {
                if (heightMeasureSpecMode != MeasureSpec.EXACTLY) {
                    height = Math.round(width / mAspectRatio);

                    if (heightMeasureSpecMode == MeasureSpec.AT_MOST) {
                        height = Math.min(height, heightMeasureSpecSize);
                    }
                }
            } else if (heightMeasureSpecMode == MeasureSpec.EXACTLY) {
                width = Math.round(height * mAspectRatio);

                if (widthMeasureSpecMode == MeasureSpec.AT_MOST) {
                    width = Math.min(width, widthMeasureSpecSize);
                }
            } else if (widthMeasureSpecMode == MeasureSpec.UNSPECIFIED) {
                width = Math.round(height * mAspectRatio);
            } else if (heightMeasureSpecMode == MeasureSpec.UNSPECIFIED) {
                height = Math.round(width / mAspectRatio);
            } else {
                final int aspectRatioHeight = Math.round(width / mAspectRatio);
                final int aspectRatioWidth = Math.round(height * mAspectRatio);

                if (aspectRatioHeight < heightMeasureSpecSize) {
                    height = aspectRatioHeight;
                } else if (aspectRatioWidth < widthMeasureSpecSize) {
                    width = aspectRatioWidth;
                }
            }

            super.onMeasure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
        }
    }

    public float getAspectRatio() {
        return mAspectRatio;
    }

    public void setAspectRatio(final float aspectRatio) {
        mAspectRatio = Math.max(0F, aspectRatio);
        invalidate();
    }

}
