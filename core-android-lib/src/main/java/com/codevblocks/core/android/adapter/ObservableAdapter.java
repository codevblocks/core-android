package com.codevblocks.core.android.adapter;

public interface ObservableAdapter {

    void notifyDataSetChanged();

    void notifyDataSetInvalidated();

}
