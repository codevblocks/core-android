package com.codevblocks.core.android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.codevblocks.core.android.R;

public class CDBConstraintLayout extends ConstraintLayout {

    private final float DEFAULT_ASPECT_RATIO = 0F;
    private final float DEFAULT_ASPECT_RATIO_MAX_HEIGHT = 0F;

    private float mAspectRatio;
    private float mAspectRatioMaxHeight;

    public CDBConstraintLayout(final Context context) {
        this(context, null, 0);
    }

    public CDBConstraintLayout(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CDBConstraintLayout(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initialize(context, attrs);
    }

    private final void initialize(final Context context, final AttributeSet attrs) {
        this.mAspectRatio = DEFAULT_ASPECT_RATIO;
        this.mAspectRatioMaxHeight = DEFAULT_ASPECT_RATIO_MAX_HEIGHT;

        final TypedArray typedArray = attrs != null ? context.getTheme().obtainStyledAttributes(attrs, R.styleable.CDBConstraintLayout, 0, 0) : null;

        if (attrs != null) {
            try {
                this.mAspectRatio = typedArray.getFloat(R.styleable.CDBConstraintLayout_aspectRatio, mAspectRatio);
                this.mAspectRatioMaxHeight = typedArray.getFloat(R.styleable.CDBConstraintLayout_aspectRatio_maxHeight, mAspectRatioMaxHeight);
            } finally {
                typedArray.recycle();
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        final int measuredWidth = getMeasuredWidth();
        final int measuredHeight = getMeasuredHeight();

        if (Float.compare(0F, mAspectRatio) != 0) {
            final int widthMeasureSpecMode = MeasureSpec.getMode(widthMeasureSpec);
            final int widthMeasureSpecSize = MeasureSpec.getSize(widthMeasureSpec);
            final int heightMeasureSpecMode = MeasureSpec.getMode(heightMeasureSpec);
            final int heightMeasureSpecSize = MeasureSpec.getSize(heightMeasureSpec);

            int width = measuredWidth;
            int height = measuredHeight;

            if (widthMeasureSpecMode == MeasureSpec.EXACTLY) {
                if (heightMeasureSpecMode != MeasureSpec.EXACTLY) {
                    height = Math.round(width / mAspectRatio);

                    if (heightMeasureSpecMode == MeasureSpec.AT_MOST) {
                        height = Math.min(height, heightMeasureSpecSize);
                    }
                }
            } else if (heightMeasureSpecMode == MeasureSpec.EXACTLY) {
                width = Math.round(height * mAspectRatio);

                if (widthMeasureSpecMode == MeasureSpec.AT_MOST) {
                    width = Math.min(width, widthMeasureSpecSize);
                }
            } else if (widthMeasureSpecMode == MeasureSpec.UNSPECIFIED) {
                width = Math.round(height * mAspectRatio);
            } else if (heightMeasureSpecMode == MeasureSpec.UNSPECIFIED) {
                height = Math.round(width / mAspectRatio);
            } else {
                final int aspectRatioHeight = Math.round(width / mAspectRatio);
                final int aspectRatioWidth = Math.round(height * mAspectRatio);

                if (aspectRatioHeight < heightMeasureSpecSize) {
                    height = aspectRatioHeight;
                } else if (aspectRatioWidth < widthMeasureSpecSize) {
                    width = aspectRatioWidth;
                }
            }

            super.onMeasure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
        } else if (Float.compare(0F, mAspectRatioMaxHeight) != 0) {
            super.onMeasure(
                    MeasureSpec.makeMeasureSpec(measuredWidth, MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(Math.min(measuredHeight, Math.round(measuredWidth / mAspectRatioMaxHeight)), MeasureSpec.AT_MOST));
        }
    }

    public float getAspectRatio() {
        return mAspectRatio;
    }

    public void setAspectRatio(final float aspectRatio) {
        mAspectRatio = aspectRatio;
    }

    public float getAspectRatioMaxHeight() {
        return mAspectRatioMaxHeight;
    }

    public void setAspectRatioMaxHeight(final float aspectRatioMaxHeight) {
        mAspectRatioMaxHeight = aspectRatioMaxHeight;
        invalidate();
    }

}
