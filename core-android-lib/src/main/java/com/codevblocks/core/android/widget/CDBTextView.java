package com.codevblocks.core.android.widget;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class CDBTextView extends AppCompatTextView implements CDBWidget {

    public CDBTextView(final Context context) {
        super(context);
    }

    public CDBTextView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public CDBTextView(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
