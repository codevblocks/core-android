package com.codevblocks.core.android.adapter;

import android.content.Context;
import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public abstract class BaseListAdapter<T> implements ListAdapter, ObservableAdapter {

    private final Context mContext;
    private final LayoutInflater mLayoutInflater;

    private final DataSetObservable mDataSetObservable;
    private final List<T> mData;

    public BaseListAdapter(@NonNull final Context context, @Nullable final List<T> data) {
        this.mContext = context;
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.mDataSetObservable = new DataSetObservable();
        this.mData = data;
    }

    protected final Context getContext() {
        return mContext;
    }

    protected final LayoutInflater getLayoutInflater() {
        return mLayoutInflater;
    }

    protected final List<T> getData() {
        return mData;
    }

    @Override
    public boolean isEmpty() {
        return mData == null || mData.size() == 0;
    }

    @Override
    public int getCount() {
        return mData != null ? mData.size() : 0;
    }

    @Override
    public T getItem(final int position) {
        return mData.get(position);
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(final int position) {
        return true;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public int getItemViewType(final int position) {
        return 0;
    }

    public abstract View getView(int position, View convertView, ViewGroup parent);

    @Override
    public void registerDataSetObserver(final DataSetObserver observer) {
        mDataSetObservable.registerObserver(observer);
    }

    @Override
    public void unregisterDataSetObserver(final DataSetObserver observer) {
        mDataSetObservable.unregisterObserver(observer);
    }
    @Override
    public void notifyDataSetChanged() {
        mDataSetObservable.notifyChanged();
    }

    @Override
    public void notifyDataSetInvalidated() {
        mDataSetObservable.notifyInvalidated();
    }

}
