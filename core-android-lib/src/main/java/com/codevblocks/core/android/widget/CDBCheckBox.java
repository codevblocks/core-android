package com.codevblocks.core.android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.appcompat.widget.AppCompatCheckBox;

import com.codevblocks.core.android.R;
import com.codevblocks.core.android.util.CompatUtils;
import com.codevblocks.core.android.util.ViewUtils;

public class CDBCheckBox extends AppCompatCheckBox implements CDBWidget {

    @IdRes
    private int mErrorTextViewResId;

    private CharSequence mError;

    public CDBCheckBox(final Context context) {
        this(context, null, 0);
    }

    public CDBCheckBox(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CDBCheckBox(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initialize(context, attrs);
    }

    private final void initialize(final Context context, final AttributeSet attrs) {
        this.mErrorTextViewResId = CompatUtils.Resources.ID_NULL;

        final TypedArray typedArray = attrs != null ? context.getTheme().obtainStyledAttributes(attrs, R.styleable.CDBCheckBox, 0, 0) : null;

        if (typedArray != null) {
            try {
                final int errorTextViewResId = typedArray.getResourceId(R.styleable.CDBCheckBox_errorTextView, 0);
                if (errorTextViewResId != 0) {
                    final String resourceType = context.getResources().getResourceTypeName(errorTextViewResId);
                    if ("id".equals(resourceType)) {
                        mErrorTextViewResId = errorTextViewResId;
                    }
                }
            } finally {
                typedArray.recycle();
            }
        }
    }

    @Override
    protected int[] onCreateDrawableState(final int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);

        if (mError != null) {
            mergeDrawableStates(drawableState, STATE_ERROR);
        }

        return drawableState;
    }

    @Override
    public void setError(final CharSequence error, final Drawable icon) {
        this.mError = error != null && error.length() > 0 ? error : null;

        if (mErrorTextViewResId == 0) {
            super.setError(error, icon);
        } else {
            final View topLevelParentView = ViewUtils.getTopLevelParent(this);
            final TextView errorTextView = topLevelParentView.findViewById(mErrorTextViewResId);
            errorTextView.setText(mError);
        }

        refreshDrawableState();
    }

}
