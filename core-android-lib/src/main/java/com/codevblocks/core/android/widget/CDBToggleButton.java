package com.codevblocks.core.android.widget;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatToggleButton;

public class CDBToggleButton extends AppCompatToggleButton implements CDBWidget {

    public CDBToggleButton(final Context context) {
        super(context);
    }

    public CDBToggleButton(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public CDBToggleButton(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
