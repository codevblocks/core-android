package com.codevblocks.core.android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

import com.codevblocks.core.android.R;

public class CDBImageView extends AppCompatImageView implements CDBWidget {

    private static final float DEFAULT_ASPECT_RATIO = 0F;
    private static final int DEFAULT_OVERLAY_COLOR = 0;
    private final static int DEFAULT_OVERLAY_COLOR_ALPHA = 0;
    private static final int DEFAULT_BLUR_RADIUS = 0;

    public static final int MIN_BLUR_RADIUS = 0;
    public static final int MAX_BLUR_RADIUS = 25;

    private float mAspectRatio;

    private int mOverlayColor;
    private int mOverlayColorAlpha;

    private int mBlurRadius;

    private Bitmap mOffscreenBitmap;
    private Canvas mOffscreenCanvas;

    private Allocation mBlurInputAllocation;
    private Allocation mBlurOutputAllocation;

    private final Paint mPaint;

    private final RenderScript mRenderScript;
    private final ScriptIntrinsicBlur mBlurScript;

    public CDBImageView(final Context context) {
        this(context, null, 0);
    }

    public CDBImageView(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CDBImageView(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initialize(context, attrs);

        mPaint = new Paint();

        if (isInEditMode()) {
            mRenderScript = null;
            mBlurScript = null;
        } else {
            mRenderScript = RenderScript.create(context);
            mBlurScript = ScriptIntrinsicBlur.create(mRenderScript, Element.U8_4(mRenderScript));
        }
    }

    private final void initialize(final Context context, final AttributeSet attrs) {
        this.mAspectRatio = DEFAULT_ASPECT_RATIO;

        this.mOverlayColor = DEFAULT_OVERLAY_COLOR;
        this.mOverlayColorAlpha = DEFAULT_OVERLAY_COLOR_ALPHA;

        this.mBlurRadius = DEFAULT_BLUR_RADIUS;

        final TypedArray typedArray = attrs != null ? context.getTheme().obtainStyledAttributes(attrs, R.styleable.CDBImageView, 0, 0) : null;

        if (attrs != null) {
            try {
                setAspectRatio(typedArray.getFloat(R.styleable.CDBImageView_aspectRatio, mAspectRatio));
                setOverlayColor(typedArray.getColor(R.styleable.CDBImageView_overlayColor, mOverlayColor));
                setOverlayColorAlpha(typedArray.getFloat(R.styleable.CDBImageView_overlayColorAlpha, mOverlayColorAlpha));
                setBlurRadius(typedArray.getInt(R.styleable.CDBImageView_blurRadius, mBlurRadius));
            } finally {
                typedArray.recycle();
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        final int measuredWidth = getMeasuredWidth();
        final int measuredHeight = getMeasuredHeight();

        if (Float.compare(0F, mAspectRatio) != 0) {
            final int widthMeasureSpecMode = MeasureSpec.getMode(widthMeasureSpec);
            final int widthMeasureSpecSize = MeasureSpec.getSize(widthMeasureSpec);
            final int heightMeasureSpecMode = MeasureSpec.getMode(heightMeasureSpec);
            final int heightMeasureSpecSize = MeasureSpec.getSize(heightMeasureSpec);

            int width = measuredWidth;
            int height = measuredHeight;

            if (widthMeasureSpecMode == MeasureSpec.EXACTLY) {
                if (heightMeasureSpecMode != MeasureSpec.EXACTLY) {
                    height = Math.round(width / mAspectRatio);

                    if (heightMeasureSpecMode == MeasureSpec.AT_MOST) {
                        height = Math.min(height, heightMeasureSpecSize);
                    }
                }
            } else if (heightMeasureSpecMode == MeasureSpec.EXACTLY) {
                width = Math.round(height * mAspectRatio);

                if (widthMeasureSpecMode == MeasureSpec.AT_MOST) {
                    width = Math.min(width, widthMeasureSpecSize);
                }
            } else if (widthMeasureSpecMode == MeasureSpec.UNSPECIFIED) {
                width = Math.round(height * mAspectRatio);
            } else if (heightMeasureSpecMode == MeasureSpec.UNSPECIFIED) {
                height = Math.round(width / mAspectRatio);
            } else {
                final int aspectRatioHeight = Math.round(width / mAspectRatio);
                final int aspectRatioWidth = Math.round(height * mAspectRatio);

                if (aspectRatioHeight < heightMeasureSpecSize) {
                    height = aspectRatioHeight;
                } else if (aspectRatioWidth < widthMeasureSpecSize) {
                    width = aspectRatioWidth;
                }
            }

            super.onMeasure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
        }
    }

    public float getAspectRatio() {
        return mAspectRatio;
    }

    public void setAspectRatio(final float aspectRatio) {
        mAspectRatio = Math.max(0F, aspectRatio);
        invalidate();
    }

    public int getOverlayColor() {
        return mOverlayColor;
    }

    public void setOverlayColor(final int overlayColor) {
        mOverlayColor = overlayColor;
        invalidate();
    }

    public int getOverlayColorAlpha() {
        return mOverlayColorAlpha;
    }

    public void setOverlayColorAlpha(final float overlayColorAlpha) {
        setOverlayColorAlpha((int) (255F * Math.max(0F, Math.min(1F, overlayColorAlpha))));
    }

    public void setOverlayColorAlpha(final int overlayColorAlpha) {
        mOverlayColorAlpha = Math.max(0, Math.min(255, overlayColorAlpha));
        invalidate();
    }

    public int getBlurRadius() {
        return mBlurRadius;
    }

    public void setBlurRadius(final int blurRadius) {
        mBlurRadius = Math.max(MIN_BLUR_RADIUS, Math.min(MAX_BLUR_RADIUS, blurRadius));
        invalidate();
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        final Canvas drawCanvas;

        final boolean blur = mBlurRadius != 0 && !isInEditMode();
        final boolean drawOffscreen = blur;

        if (drawOffscreen) {
            if (mOffscreenBitmap == null || mOffscreenBitmap.getWidth() != getWidth() || mOffscreenBitmap.getHeight() != getHeight()) {
                mOffscreenBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
                mOffscreenCanvas = new Canvas(mOffscreenBitmap);

                if (mBlurInputAllocation != null) {
                    mBlurInputAllocation.destroy();
                    mBlurInputAllocation = null;
                }

                if (mBlurOutputAllocation != null) {
                    mBlurOutputAllocation.destroy();
                    mBlurOutputAllocation = null;
                }
            }

            if (blur && (mBlurInputAllocation == null || mBlurOutputAllocation == null)) {
                mBlurInputAllocation = Allocation.createFromBitmap(mRenderScript, mOffscreenBitmap);
                mBlurOutputAllocation = Allocation.createTyped(mRenderScript, mBlurInputAllocation.getType());
            }

            drawCanvas = mOffscreenCanvas;
        } else {
            mOffscreenBitmap = null;
            mOffscreenCanvas = null;

            if (mBlurInputAllocation != null) {
                mBlurInputAllocation.destroy();
                mBlurInputAllocation = null;
            }

            if (mBlurOutputAllocation != null) {
                mBlurOutputAllocation.destroy();
                mBlurOutputAllocation = null;
            }

            drawCanvas = canvas;
        }

        super.onDraw(drawCanvas);

        if (mOverlayColorAlpha > 0) {
            mPaint.setAntiAlias(false);
            mPaint.setStyle(Paint.Style.FILL);
            mPaint.setColor(mOverlayColor);
            mPaint.setAlpha(mOverlayColorAlpha);

            drawCanvas.drawRect(0, 0, getWidth(), getHeight(), mPaint);
        }

        if (blur) {
            mBlurInputAllocation.copyFrom(mOffscreenBitmap);

            mBlurScript.setRadius(mBlurRadius);
            mBlurScript.setInput(mBlurInputAllocation);
            mBlurScript.forEach(mBlurOutputAllocation);

            mBlurOutputAllocation.copyTo(mOffscreenBitmap);
        }

        if (drawOffscreen) {
            canvas.drawBitmap(mOffscreenBitmap, 0, 0, null);
        }
    }

}
