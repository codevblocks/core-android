package com.codevblocks.core.android.widget;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatSeekBar;

public class CDBSeekBar extends AppCompatSeekBar implements CDBWidget {

    public CDBSeekBar(final Context context) {
        super(context);
    }

    public CDBSeekBar(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public CDBSeekBar(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
