package com.codevblocks.core.android.widget.filter;

import android.widget.Filter;

import java.util.IdentityHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;

@SuppressWarnings("WeakerAccess")
public abstract class SimpleFilter<T, FT> extends Filter {

    public static final int MATCH_NONE = 0;
    public static final int MATCH_PARTIAL = 1;
    public static final int MATCH_COMPLETE = 2;

    private final List<T> mValues;

    private final FilterStrategy<FT> mFilterStrategy;
    private final int mFilterMatch;

    private final Map<T, FT> mValueFilterObjects;

    public SimpleFilter(final List<T> values, final FilterStrategy<FT> filterStrategy, final int filterMatch) {
        this.mValues = values;

        this.mFilterStrategy = filterStrategy;
        this.mFilterMatch = filterMatch;

        this.mValueFilterObjects = new IdentityHashMap<>();
    }

    protected abstract boolean hasConstantFilterValues();
    protected abstract FT toFilterObject(@NonNull T value);

    @Override
    protected FilterResults performFiltering(final CharSequence constraint) {
        final FilterResults results = new FilterResults();

        if (mValues == null || mValues.size() == 0) {
            results.values = null;
            results.count = 0;
        } else {
            final List<ValueMatch<T>> filteredValues = getFilteredValues(constraint);

            results.values = filteredValues;
            results.count = filteredValues.size();
        }

        return results;
    }

    protected List<ValueMatch<T>> getFilteredValues(final CharSequence constraint) {
        final List<ValueMatch<T>> filteredValues = new LinkedList<>();

        if (constraint == null || constraint.length() == 0) {
            for (T value : mValues) {
                filteredValues.add(new ValueMatch<T>(value, MATCH_PARTIAL));
            }
        } else {
            if (mFilterStrategy == null) {
                for (T value : mValues) {
                    filteredValues.add(new ValueMatch<T>(value, MATCH_PARTIAL));
                }
            } else {
                final CharSequence filterConstraint = mFilterStrategy.transformsConstraint() ?
                        mFilterStrategy.transformConstraint(constraint) : constraint;

                final boolean bHasConstantFilterValues = hasConstantFilterValues();

                FT valueFilterObject;
                int valueFilterMatch;

                for (T value : mValues) {
                    if (bHasConstantFilterValues) {
                        valueFilterObject = mValueFilterObjects.get(value);

                        if (valueFilterObject == null) {
                            valueFilterObject = mFilterStrategy.transformsValues() ?
                                    mFilterStrategy.transformValue(toFilterObject(value)) : toFilterObject(value);

                            mValueFilterObjects.put(value, valueFilterObject);
                        }
                    } else {
                        valueFilterObject = mFilterStrategy.transformsValues() ?
                                mFilterStrategy.transformValue(toFilterObject(value)) : toFilterObject(value);
                    }

                    valueFilterMatch = mFilterStrategy.matches(valueFilterObject, filterConstraint);
                    if (mFilterMatch <= valueFilterMatch) {
                        filteredValues.add(new ValueMatch<T>(value, valueFilterMatch));
                    }
                }
            }
        }

        return filteredValues;
    }

    public static final class ValueMatch<T> {

        public final T value;
        public final int filterMatch;

        public ValueMatch(final T value, final int filterMatch) {
            this.value = value;
            this.filterMatch = filterMatch;
        }

    }

    public interface FilterStrategy<FT> {

        boolean transformsConstraint();
        CharSequence transformConstraint(@NonNull CharSequence constraint);

        boolean transformsValues();
        FT transformValue(@NonNull FT value);

        int matches(@NonNull FT value, @NonNull CharSequence constraint);

    }

    public static abstract class BaseFilterStrategy<FT> implements FilterStrategy<FT> {

        @Override
        public boolean transformsConstraint() {
            return false;
        }

        @Override
        public CharSequence transformConstraint(@NonNull final CharSequence constraint) {
            return constraint;
        }

        @Override
        public boolean transformsValues() {
            return false;
        }

        @Override
        public FT transformValue(@NonNull final FT value) {
            return value;
        }

    }

    public static final class StringFilterStrategy extends BaseFilterStrategy<String> {

        @Override
        public final int matches(@NonNull final String value, @NonNull final CharSequence constraint) {
            return value.equals(constraint) ?
                    MATCH_COMPLETE :
                    value.contains(constraint) ?
                            MATCH_PARTIAL :
                            MATCH_NONE;
        }

    }

    public static final class FlexibleStringFilterStrategy implements FilterStrategy<String> {

        private static final String REGEX_NON_ALPHA_NUMERIC = "[^a-zA-Z0-9]";

        @Override
        public final boolean transformsConstraint() {
            return true;
        }

        @Override
        public final CharSequence transformConstraint(@NonNull final CharSequence constraint) {
            return constraint.toString().replaceAll(REGEX_NON_ALPHA_NUMERIC, "").toLowerCase();
        }

        @Override
        public final boolean transformsValues() {
            return true;
        }

        @Override
        public String transformValue(@NonNull final String value) {
            return value.replaceAll(REGEX_NON_ALPHA_NUMERIC, "").toLowerCase();
        }

        @Override
        public int matches(@NonNull final String value, @NonNull final CharSequence constraint) {
            return value.equals(constraint) ?
                    MATCH_COMPLETE :
                    value.contains(constraint) ?
                            MATCH_PARTIAL :
                            MATCH_NONE;
        }

    }

}
