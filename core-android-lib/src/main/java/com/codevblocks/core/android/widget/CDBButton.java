package com.codevblocks.core.android.widget;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatButton;

public class CDBButton extends AppCompatButton implements CDBWidget {

    public CDBButton(final Context context) {
        super(context);
    }

    public CDBButton(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public CDBButton(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
