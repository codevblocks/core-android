package com.codevblocks.core.android.preference;

import android.content.Context;
import android.content.SharedPreferences;

public final class PreferencesRepository {

    private final SharedPreferences mPreferences;

    public PreferencesRepository(final Context context, final String name) {
        this(context, name, Context.MODE_PRIVATE);
    }

    public PreferencesRepository(final Context context, final String name, final int mode) {
        this.mPreferences = context.getSharedPreferences(name, mode);
    }

    public final int get(final String key, final int defaultValue) {
        return mPreferences.getInt(key, defaultValue);
    }

    public final long get(final String key, final long defaultValue) {
        return mPreferences.getLong(key, defaultValue);
    }

    public final float get(final String key, final float defaultValue) {
        return mPreferences.getFloat(key, defaultValue);
    }

    public final boolean get(final String key, final boolean defaultValue) {
        return mPreferences.getBoolean(key, defaultValue);
    }

    public final String get(final String key, final String defaultValue) {
        return mPreferences.getString(key, defaultValue);
    }

    public final void put(final String key, final int value) {
        mPreferences.edit().putInt(key, value).apply();
    }

    public final void put(final String key, final long value) {
        mPreferences.edit().putLong(key, value).apply();
    }

    public final void put(final String key, final float value) {
        mPreferences.edit().putFloat(key, value).apply();
    }

    public final void put(final String key, final boolean value) {
        mPreferences.edit().putBoolean(key, value).apply();
    }

    public final void put(final String key, final String value) {
        mPreferences.edit().putString(key, value).apply();
    }

    public final void remove(final String key) {
        mPreferences.edit().remove(key).apply();
    }

    public final void clear() {
        mPreferences.edit().clear().apply();
    }

}
