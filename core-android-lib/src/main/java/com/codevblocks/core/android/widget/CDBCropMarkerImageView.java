package com.codevblocks.core.android.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.Region;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MotionEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.codevblocks.core.android.R;

import java.util.Arrays;
import java.util.List;

public class CDBCropMarkerImageView extends CDBImageView {

    public static final int CROP_MASK_RECTANGLE = 0;
    public static final int CROP_MASK_OVAL = 1;

    public static final int CROP_MARKER_GRID_LINES_STYLE_NONE = 0;
    public static final int CROP_MARKER_GRID_LINES_STYLE_TOUCH = 1;
    public static final int CROP_MARKER_GRID_LINES_STYLE_ALWAYS = 2;

    private static final ScaleType SCALE_TYPE = ScaleType.FIT_CENTER;

    private static final boolean DEFAULT_CROP_MARKER_ENABLED = false;
    private static final float DEFAULT_CROP_MARKER_ASPECT_RATIO = Float.NaN;
    private static final float DEFAULT_CROP_MARKER_MIN_SIZE_DP = 50;
    private static final int DEFAULT_CROP_MARKER_BACKGROUND_COLOR = 0x80000000;
    private static final int DEFAULT_CROP_MARKER_STROKE_COLOR = 0x7FFFFFFF;
    private static final int DEFAULT_CROP_MARKER_STROKE_WIDTH_DP = 1;
    private static final int DEFAULT_CROP_MARKER_TOUCH_HANDLE_STROKE_COLOR = DEFAULT_CROP_MARKER_STROKE_COLOR;
    private static final int DEFAULT_CROP_MARKER_TOUCH_HANDLE_STROKE_WIDTH_DP = DEFAULT_CROP_MARKER_STROKE_WIDTH_DP;
    private static final int DEFAULT_CROP_MARKER_TOUCH_HANDLE_STROKE_LENGTH_DP = 20;
    private static final int DEFAULT_CROP_MARKER_TOUCH_HANDLE_INSET_DP = 0;
    private static final int DEFAULT_CROP_MARKER_TOUCH_THRESHOLD_DP = 10;
    private static final int DEFAULT_CROP_MARKER_GRID_LINES_STYLE = CROP_MARKER_GRID_LINES_STYLE_TOUCH;
    private static final int DEFAULT_CROP_MARKER_GRID_LINES = 1;
    private static final int DEFAULT_CROP_MARKER_GRID_LINE_COLOR = DEFAULT_CROP_MARKER_STROKE_COLOR;
    private static final int DEFAULT_CROP_MARKER_GRID_LINE_WIDTH_DP = 1;
    private static final int DEFAULT_CROP_MARKER_MASK = CROP_MASK_RECTANGLE;

    private boolean mCropMarkerEnabled;
    private float mCropMarkerAspectRatio;
    private int mCropMarkerMinSize;
    private int mCropMarkerBackgroundColor;
    private int mCropMarkerStrokeColor;
    private int mCropMarkerStrokeWidth;
    private int mCropMarkerTouchHandleStrokeColor;
    private int mCropMarkerTouchHandleStrokeWidth;
    private int mCropMarkerTouchHandleStrokeLength;
    private int mCropMarkerTouchHandleInset;
    private int mCropMarkerTouchThreshold;
    private int mCropMarkerGridLinesStyle;
    private int mCropMarkerGridLines;
    private int mCropMarkerGridLineColor;
    private int mCropMarkerGridLineWidth;
    private int mCropMarkerMask;

    private final MInt mViewLeft = new MInt(0);
    private final MInt mViewTop = new MInt(0);
    private final MInt mViewRight = new MInt(0);
    private final MInt mViewBottom = new MInt(0);

    private final MInt mCropMarkerLeft = new MInt(0);
    private final MInt mCropMarkerTop = new MInt(0);
    private final MInt mCropMarkerRight = new MInt(0);
    private final MInt mCropMarkerBottom = new MInt(0);

    private final Point mCropMarkerTopLeft = new Point(mCropMarkerLeft, mCropMarkerTop);
    private final Point mCropMarkerTopRight = new Point(mCropMarkerRight, mCropMarkerTop);
    private final Point mCropMarkerBottomLeft = new Point(mCropMarkerLeft, mCropMarkerBottom);
    private final Point mCropMarkerBottomRight = new Point(mCropMarkerRight, mCropMarkerBottom);

    private final MFloat mCropAspectRatio = new MFloat(0F);
    private final MInt mCropMinSize = new MInt(0);
    private final MInt mCropHandleTouchThreshold = new MInt(0);

    private final PointTouchHandle mCropMarkerTopLeftHandle = new PointTouchHandle(mCropMarkerTopLeft, mCropHandleTouchThreshold,
            new Bounds() {
                @Override final int left() { return mViewLeft.value; }
                @Override final int top() { return mViewTop.value; }
                @Override final int right() { return Math.max(mCropMarkerRight.value - mCropMinSize.value, left()); }
                @Override final int bottom() { return Math.max(mCropMarkerBottom.value - mCropMinSize.value, top()); }
            },
            mCropAspectRatio, mCropMarkerBottomRight);

    private final PointTouchHandle mCropMarkerTopRightHandle = new PointTouchHandle(mCropMarkerTopRight, mCropHandleTouchThreshold,
            new Bounds() {
                @Override final int left() { return Math.min(mCropMarkerLeft.value + mCropMinSize.value, right()); }
                @Override final int top() { return mViewTop.value; }
                @Override final int right() { return mViewRight.value; }
                @Override final int bottom() { return Math.max(mCropMarkerBottom.value - mCropMinSize.value, top()); }
            },
            mCropAspectRatio, mCropMarkerBottomLeft);

    private final PointTouchHandle mCropMarkerBottomLeftHandle = new PointTouchHandle(mCropMarkerBottomLeft, mCropHandleTouchThreshold,
            new Bounds() {
                @Override final int left() { return mViewLeft.value; }
                @Override final int top() { return Math.min(mCropMarkerTop.value + mCropMinSize.value, bottom()); }
                @Override final int right() { return Math.max(mCropMarkerRight.value - mCropMinSize.value, left()); }
                @Override final int bottom() { return mViewBottom.value; }
            },
            mCropAspectRatio, mCropMarkerTopRight);

    private final PointTouchHandle mCropMarkerBottomRightHandle = new PointTouchHandle(mCropMarkerBottomRight, mCropHandleTouchThreshold,
            new Bounds() {
                @Override final int left() { return Math.min(mCropMarkerLeft.value + mCropMinSize.value, right()); }
                @Override final int top() { return Math.min(mCropMarkerTop.value + mCropMinSize.value, bottom()); }
                @Override final int right() { return mViewRight.value; }
                @Override final int bottom() { return mViewBottom.value; }
            },
            mCropAspectRatio, mCropMarkerTopLeft);

    private final AreaTouchHandle mCropMarkerAreaTouchHandle = new AreaTouchHandle(mCropMarkerLeft, mCropMarkerTop, mCropMarkerRight, mCropMarkerBottom,
            new Bounds() {
                @Override final int left() { return mViewLeft.value; }
                @Override final int top() { return mViewTop.value; }
                @Override final int right() { return left() + (mViewRight.value - left()) - (mCropMarkerRight.value - mCropMarkerLeft.value); }
                @Override final int bottom() { return top() + (mViewBottom.value - top()) - (mCropMarkerBottom.value - mCropMarkerTop.value); }
    });

    private final List<TouchHandle> mCropMarkerTouchHandles = Arrays.asList(
            mCropMarkerTopLeftHandle,
            mCropMarkerTopRightHandle,
            mCropMarkerBottomLeftHandle,
            mCropMarkerBottomRightHandle,
            mCropMarkerAreaTouchHandle
    );

    private int mActiveTouchPointerId = MotionEvent.INVALID_POINTER_ID;
    private TouchHandle mActiveTouchHandle = null;

    private final Path mMaskPath = new Path();
    private final RectF mCropDrawRect = new RectF();
    private final RectF mCropTouchHandlesRect = new RectF();
    private final Paint mPaint = new Paint();

    public CDBCropMarkerImageView(Context context) {
        this(context, null, 0);
    }

    public CDBCropMarkerImageView(Context context, AttributeSet attrs) {
        this (context, attrs, 0);
    }

    public CDBCropMarkerImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        super.setAdjustViewBounds(true);
        super.setScaleType(SCALE_TYPE);

        final Resources resources = getResources();
        final DisplayMetrics displayMetrics = resources.getDisplayMetrics();

        this.mCropMarkerEnabled = DEFAULT_CROP_MARKER_ENABLED;
        this.mCropMarkerAspectRatio = DEFAULT_CROP_MARKER_ASPECT_RATIO;
        this.mCropMarkerMinSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_CROP_MARKER_MIN_SIZE_DP, displayMetrics);
        this.mCropMarkerBackgroundColor = DEFAULT_CROP_MARKER_BACKGROUND_COLOR;
        this.mCropMarkerStrokeColor = DEFAULT_CROP_MARKER_STROKE_COLOR;
        this.mCropMarkerStrokeWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_CROP_MARKER_STROKE_WIDTH_DP, displayMetrics);
        this.mCropMarkerTouchHandleStrokeColor = DEFAULT_CROP_MARKER_TOUCH_HANDLE_STROKE_COLOR;
        this.mCropMarkerTouchHandleStrokeWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_CROP_MARKER_TOUCH_HANDLE_STROKE_WIDTH_DP, displayMetrics);
        this.mCropMarkerTouchHandleStrokeLength = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_CROP_MARKER_TOUCH_HANDLE_STROKE_LENGTH_DP, displayMetrics);
        this.mCropMarkerTouchHandleInset = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_CROP_MARKER_TOUCH_HANDLE_INSET_DP, displayMetrics);
        this.mCropMarkerTouchThreshold = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_CROP_MARKER_TOUCH_THRESHOLD_DP, displayMetrics);
        this.mCropMarkerGridLinesStyle = DEFAULT_CROP_MARKER_GRID_LINES_STYLE;
        this.mCropMarkerGridLines = DEFAULT_CROP_MARKER_GRID_LINES;
        this.mCropMarkerGridLineColor = DEFAULT_CROP_MARKER_GRID_LINE_COLOR;
        this.mCropMarkerGridLineWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_CROP_MARKER_GRID_LINE_WIDTH_DP, displayMetrics);
        this.mCropMarkerMask = DEFAULT_CROP_MARKER_MASK;

        final TypedArray typedArray = attrs != null ? context.getTheme().obtainStyledAttributes(attrs, R.styleable.CDBCropMarkerImageView, 0, 0) : null;

        if (typedArray != null) {
            try {
                this.mCropMarkerEnabled = typedArray.getBoolean(R.styleable.CDBCropMarkerImageView_cropMarkerEnabled, mCropMarkerEnabled);
                this.mCropMarkerAspectRatio = typedArray.getFloat(R.styleable.CDBCropMarkerImageView_cropMarkerAspectRatio, mCropMarkerAspectRatio);
                this.mCropMarkerMinSize = typedArray.getDimensionPixelSize(R.styleable.CDBCropMarkerImageView_cropMarkerMinSize, mCropMarkerMinSize);
                this.mCropMarkerBackgroundColor = typedArray.getColor(R.styleable.CDBCropMarkerImageView_cropMarkerBackgroundColor, mCropMarkerBackgroundColor);
                this.mCropMarkerStrokeColor = typedArray.getColor(R.styleable.CDBCropMarkerImageView_cropMarkerStrokeColor, mCropMarkerStrokeColor);
                this.mCropMarkerStrokeWidth = typedArray.getDimensionPixelSize(R.styleable.CDBCropMarkerImageView_cropMarkerStrokeWidth, mCropMarkerStrokeWidth);
                this.mCropMarkerTouchHandleStrokeColor = typedArray.getColor(R.styleable.CDBCropMarkerImageView_cropMarkerTouchHandleStrokeColor, mCropMarkerTouchHandleStrokeColor);
                this.mCropMarkerTouchHandleStrokeWidth = typedArray.getDimensionPixelSize(R.styleable.CDBCropMarkerImageView_cropMarkerTouchHandleStrokeWidth, mCropMarkerTouchHandleStrokeWidth);
                this.mCropMarkerTouchHandleStrokeLength = typedArray.getDimensionPixelSize(R.styleable.CDBCropMarkerImageView_cropMarkerTouchHandleStrokeLength, mCropMarkerTouchHandleStrokeLength);
                this.mCropMarkerTouchHandleInset = typedArray.getDimensionPixelSize(R.styleable.CDBCropMarkerImageView_cropMarkerTouchHandleInset, mCropMarkerTouchHandleInset);
                this.mCropMarkerTouchThreshold = typedArray.getDimensionPixelSize(R.styleable.CDBCropMarkerImageView_cropMarkerTouchThreshold, mCropMarkerTouchThreshold);
                this.mCropMarkerGridLinesStyle = typedArray.getInteger(R.styleable.CDBCropMarkerImageView_cropMarkerGridLinesStyle, mCropMarkerGridLinesStyle);
                this.mCropMarkerGridLines = Math.max(0, typedArray.getInteger(R.styleable.CDBCropMarkerImageView_cropMarkerGridLines, mCropMarkerGridLines));
                this.mCropMarkerGridLineColor = typedArray.getColor(R.styleable.CDBCropMarkerImageView_cropMarkerGridLineColor, mCropMarkerGridLineColor);
                this.mCropMarkerGridLineWidth = typedArray.getDimensionPixelSize(R.styleable.CDBCropMarkerImageView_cropMarkerGridLineWidth, mCropMarkerGridLineWidth);
                this.mCropMarkerMask = typedArray.getInteger(R.styleable.CDBCropMarkerImageView_cropMarkerMask, mCropMarkerMask);
            } finally {
                typedArray.recycle();
            }
        }

        this.mCropAspectRatio.value = mCropMarkerAspectRatio;
        this.mCropMinSize.value = mCropMarkerMinSize;
        this.mCropHandleTouchThreshold.value = mCropMarkerTouchThreshold;
    }

    @Override
    protected final void onSizeChanged(final int width, final int height, final int oldWidth, final int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);

        mViewLeft.value = 0;
        mViewTop.value = 0;
        mViewRight.value = width;
        mViewBottom.value = height;

        resetCropMarker();
    }

    @Override
    public final boolean onTouchEvent(final MotionEvent event) {
        if (!mCropMarkerEnabled) {
            return false;
        }

        final int action = event.getActionMasked();

        if (action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_POINTER_DOWN) {
            if (mActiveTouchPointerId == MotionEvent.INVALID_POINTER_ID) {
                final int pointerIndex = event.getActionIndex();
                final int pointerId = event.getPointerId(pointerIndex);

                final int eventX = Math.round(event.getX(pointerIndex));
                final int eventY = Math.round(event.getY(pointerIndex));

                TouchHandle grabbedTouchHandle = null;
                double grabbedTouchHandleMatch = Double.MAX_VALUE;

                double touchHandleMatch;
                for (TouchHandle touchHandle : mCropMarkerTouchHandles) {
                    touchHandleMatch = touchHandle.grabMatch(eventX, eventY);
                    if (Double.compare(touchHandleMatch, grabbedTouchHandleMatch) < 0) {
                        grabbedTouchHandle = touchHandle;
                        grabbedTouchHandleMatch = touchHandleMatch;
                    }
                }

                if (grabbedTouchHandle != null) {
                    mActiveTouchPointerId = pointerId;
                    mActiveTouchHandle = grabbedTouchHandle;
                    mActiveTouchHandle.grab(eventX, eventY);
                }
            }
        } else if (action == MotionEvent.ACTION_MOVE) {
            if (mActiveTouchPointerId != MotionEvent.INVALID_POINTER_ID && mActiveTouchHandle != null) {
                final int pointerIndex = event.findPointerIndex(mActiveTouchPointerId);

                mActiveTouchHandle.move(
                        Math.round(event.getX(pointerIndex)),
                        Math.round(event.getY(pointerIndex))
                );

                invalidate();
            }
        } else if (action == MotionEvent.ACTION_UP ||
                action == MotionEvent.ACTION_POINTER_UP ||
                action == MotionEvent.ACTION_OUTSIDE ||
                action == MotionEvent.ACTION_CANCEL) {
            if (mActiveTouchPointerId != MotionEvent.INVALID_POINTER_ID && mActiveTouchHandle != null) {
                if (mActiveTouchPointerId == event.getPointerId(event.getActionIndex())) {
                    mActiveTouchPointerId = MotionEvent.INVALID_POINTER_ID;
                    mActiveTouchHandle.release();
                    mActiveTouchHandle = null;

                    invalidate();
                }
            }
        }

        return true;
    }

    @Override
    @SuppressWarnings("deprecation")
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mCropMarkerEnabled) {
            final float cropMarkerStrokeWidth_half = 0.5F * mCropMarkerStrokeWidth;

            mCropDrawRect.set(
                    mCropMarkerLeft.value + cropMarkerStrokeWidth_half,
                    mCropMarkerTop.value + cropMarkerStrokeWidth_half,
                    mCropMarkerRight.value - cropMarkerStrokeWidth_half,
                    mCropMarkerBottom.value - cropMarkerStrokeWidth_half);

            mPaint.setStyle(Paint.Style.FILL);
            mPaint.setColor(mCropMarkerBackgroundColor);

            canvas.save();

            mMaskPath.reset();
            if (mCropMarkerMask == CROP_MASK_RECTANGLE) {
                mMaskPath.addRect(mCropDrawRect, Path.Direction.CW);
            } else if (mCropMarkerMask == CROP_MASK_OVAL) {
                mMaskPath.addOval(mCropDrawRect, Path.Direction.CW);
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                canvas.clipOutPath(mMaskPath);
            } else {
                canvas.clipPath(mMaskPath, Region.Op.DIFFERENCE);
            }

            canvas.drawPaint(mPaint);
            canvas.restore();

            if (mCropMarkerGridLines > 0 &&
                    mCropMarkerGridLinesStyle != CROP_MARKER_GRID_LINES_STYLE_NONE &&
                    (mCropMarkerGridLinesStyle != CROP_MARKER_GRID_LINES_STYLE_TOUCH || mActiveTouchHandle != null)) {
                mPaint.setStyle(Paint.Style.STROKE);
                mPaint.setColor(mCropMarkerGridLineColor);
                mPaint.setStrokeWidth(mCropMarkerGridLineWidth);

                final float gridCellWidth = mCropDrawRect.width() / (mCropMarkerGridLines + 1F);
                final float gridCellHeight = mCropDrawRect.height() / (mCropMarkerGridLines + 1F);

                float gridX = mCropDrawRect.left;
                float gridY = mCropDrawRect.top;

                for (int i = 0; i < mCropMarkerGridLines; ++i) {
                    gridX += gridCellWidth;
                    gridY += gridCellHeight;

                    canvas.drawLine(gridX, mCropDrawRect.top, gridX, mCropDrawRect.bottom, mPaint);
                    canvas.drawLine(mCropDrawRect.left, gridY, mCropDrawRect.right, gridY, mPaint);
                }
            }

            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setColor(mCropMarkerStrokeColor);
            mPaint.setStrokeWidth(mCropMarkerStrokeWidth);

            canvas.drawRect(mCropDrawRect, mPaint);

            if (mCropMarkerTouchHandleStrokeWidth > 0 && mCropMarkerTouchHandleStrokeLength > 0 && mCropMarkerTouchHandleStrokeColor != 0) {
                mCropTouchHandlesRect.set(
                        mCropMarkerLeft.value + mCropMarkerTouchHandleInset,
                        mCropMarkerTop.value + mCropMarkerTouchHandleInset,
                        mCropMarkerRight.value - mCropMarkerTouchHandleInset,
                        mCropMarkerBottom.value - mCropMarkerTouchHandleInset
                );

                if (mCropTouchHandlesRect.left < mCropTouchHandlesRect.right && mCropTouchHandlesRect.top < mCropTouchHandlesRect.bottom) {
                    final float cropMarkerTouchHandleStrokeWidth_half = 0.5F * mCropMarkerTouchHandleStrokeWidth;

                    mPaint.setStyle(Paint.Style.STROKE);
                    mPaint.setColor(mCropMarkerTouchHandleStrokeColor);
                    mPaint.setStrokeWidth(mCropMarkerTouchHandleStrokeWidth);

                    canvas.drawLine(mCropTouchHandlesRect.left, mCropTouchHandlesRect.top + cropMarkerTouchHandleStrokeWidth_half,
                            mCropTouchHandlesRect.left + mCropMarkerTouchHandleStrokeLength, mCropTouchHandlesRect.top + cropMarkerTouchHandleStrokeWidth_half, mPaint
                    );

                    canvas.drawLine(mCropTouchHandlesRect.left + cropMarkerTouchHandleStrokeWidth_half, mCropTouchHandlesRect.top,
                            mCropTouchHandlesRect.left + cropMarkerTouchHandleStrokeWidth_half, mCropTouchHandlesRect.top + mCropMarkerTouchHandleStrokeLength, mPaint
                    );

                    canvas.drawLine(mCropTouchHandlesRect.right, mCropTouchHandlesRect.top + cropMarkerTouchHandleStrokeWidth_half,
                            mCropTouchHandlesRect.right - mCropMarkerTouchHandleStrokeLength, mCropTouchHandlesRect.top + cropMarkerTouchHandleStrokeWidth_half, mPaint
                    );

                    canvas.drawLine(mCropTouchHandlesRect.right - cropMarkerTouchHandleStrokeWidth_half, mCropTouchHandlesRect.top,
                            mCropTouchHandlesRect.right - cropMarkerTouchHandleStrokeWidth_half, mCropTouchHandlesRect.top + mCropMarkerTouchHandleStrokeLength, mPaint
                    );

                    canvas.drawLine(mCropTouchHandlesRect.left, mCropTouchHandlesRect.bottom - cropMarkerTouchHandleStrokeWidth_half,
                            mCropTouchHandlesRect.left + mCropMarkerTouchHandleStrokeLength, mCropTouchHandlesRect.bottom - cropMarkerTouchHandleStrokeWidth_half, mPaint
                    );

                    canvas.drawLine(mCropTouchHandlesRect.left + cropMarkerTouchHandleStrokeWidth_half, mCropTouchHandlesRect.bottom,
                            mCropTouchHandlesRect.left + cropMarkerTouchHandleStrokeWidth_half, mCropTouchHandlesRect.bottom - mCropMarkerTouchHandleStrokeLength, mPaint
                    );

                    canvas.drawLine(mCropTouchHandlesRect.right, mCropTouchHandlesRect.bottom - cropMarkerTouchHandleStrokeWidth_half,
                            mCropTouchHandlesRect.right - mCropMarkerTouchHandleStrokeLength, mCropTouchHandlesRect.bottom - cropMarkerTouchHandleStrokeWidth_half, mPaint
                    );

                    canvas.drawLine(mCropTouchHandlesRect.right - cropMarkerTouchHandleStrokeWidth_half, mCropTouchHandlesRect.bottom - mCropMarkerTouchHandleStrokeLength,
                            mCropTouchHandlesRect.right - cropMarkerTouchHandleStrokeWidth_half, mCropTouchHandlesRect.bottom, mPaint
                    );
                }
            }
        }
    }

    @Override
    public final void setAdjustViewBounds(final boolean adjustViewBounds) {
        // View needs to adjust view bounds
        super.setAdjustViewBounds(true);
    }

    @Override
    public final void setScaleType(final ScaleType scaleType) {
        // Supports only one scale type
        super.setScaleType(SCALE_TYPE);
    }

    public boolean isCropMarkerEnabled() {
        return mCropMarkerEnabled;
    }

    public void setCropMarkerEnabled(final boolean cropMarkerEnabled) {
        mCropMarkerEnabled = cropMarkerEnabled;
        invalidate();
    }

    public float getCropMarkerAspectRatio() {
        return mCropMarkerAspectRatio;
    }

    public void setCropMarkerAspectRatio(final float cropMarkerAspectRatio) {
        mCropMarkerAspectRatio = Float.isNaN(cropMarkerAspectRatio) || Float.compare(cropMarkerAspectRatio, 0F) < 0 ? 0 : cropMarkerAspectRatio;
        mCropAspectRatio.value = mCropMarkerAspectRatio;

        resetCropMarker();
    }

    public int getCropMarkerMinSize() {
        return mCropMarkerMinSize;
    }

    public void setCropMarkerMinSize(final int cropMarkerMinSize) {
        mCropMarkerMinSize = cropMarkerMinSize;
        mCropMinSize.value = mCropMarkerMinSize;

        resetCropMarker();
    }

    public int getCropMarkerBackgroundColor() {
        return mCropMarkerBackgroundColor;
    }

    public void setCropMarkerBackgroundColor(final int cropMarkerBackgroundColor) {
        mCropMarkerBackgroundColor = cropMarkerBackgroundColor;
        invalidate();
    }

    public int getCropMarkerStrokeColor() {
        return mCropMarkerStrokeColor;
    }

    public void setCropMarkerStrokeColor(final int cropMarkerStrokeColor) {
        mCropMarkerStrokeColor = cropMarkerStrokeColor;
        invalidate();
    }

    public int getCropMarkerStrokeWidth() {
        return mCropMarkerStrokeWidth;
    }

    public void setCropMarkerStrokeWidth(final int cropMarkerStrokeWidth) {
        mCropMarkerStrokeWidth = cropMarkerStrokeWidth;
        invalidate();
    }

    public int getCropMarkerTouchHandleStrokeColor() {
        return mCropMarkerTouchHandleStrokeColor;
    }

    public void setCropMarkerTouchHandleStrokeColor(final int cropMarkerTouchHandleStrokeColor) {
        mCropMarkerTouchHandleStrokeColor = cropMarkerTouchHandleStrokeColor;
        invalidate();
    }

    public int getCropMarkerTouchHandleStrokeWidth() {
        return mCropMarkerTouchHandleStrokeWidth;
    }

    public void setCropMarkerTouchHandleStrokeWidth(final int cropMarkerTouchHandleStrokeWidth) {
        mCropMarkerTouchHandleStrokeWidth = cropMarkerTouchHandleStrokeWidth;
        invalidate();
    }

    public int getCropMarkerTouchHandleStrokeLength() {
        return mCropMarkerTouchHandleStrokeLength;
    }

    public void setCropMarkerTouchHandleStrokeLength(final int cropMarkerTouchHandleStrokeLength) {
        mCropMarkerTouchHandleStrokeLength = cropMarkerTouchHandleStrokeLength;
        invalidate();
    }

    public int getCropMarkerTouchHandleInset() {
        return mCropMarkerTouchHandleInset;
    }

    public void setCropMarkerTouchHandleInset(final int cropMarkerTouchHandleInset) {
        mCropMarkerTouchHandleInset = cropMarkerTouchHandleInset;
        invalidate();
    }

    public int getCropMarkerTouchThreshold() {
        return mCropMarkerTouchThreshold;
    }

    public void setCropMarkerTouchThreshold(final int cropMarkerTouchThreshold) {
        mCropMarkerTouchThreshold = cropMarkerTouchThreshold;
    }

    public int getCropMarkerGridLinesStyle() {
        return mCropMarkerGridLinesStyle;
    }

    public void setCropMarkerGridLinesStyle(final int cropMarkerGridLinesStyle) {
        mCropMarkerGridLinesStyle = cropMarkerGridLinesStyle;
        invalidate();
    }

    public int getCropMarkerGridLines() {
        return mCropMarkerGridLines;
    }

    public void setCropMarkerGridLines(final int cropMarkerGridLines) {
        mCropMarkerGridLines = cropMarkerGridLines;
        invalidate();
    }

    public int getCropMarkerGridLineColor() {
        return mCropMarkerGridLineColor;
    }

    public void setCropMarkerGridLineColor(final int cropMarkerGridLineColor) {
        mCropMarkerGridLineColor = cropMarkerGridLineColor;
        invalidate();
    }

    public int getCropMarkerGridLineWidth() {
        return mCropMarkerGridLineWidth;
    }

    public void setCropMarkerGridLineWidth(final int cropMarkerGridLineWidth) {
        mCropMarkerGridLineWidth = cropMarkerGridLineWidth;
        invalidate();
    }

    public int getCropMarkerMask() {
        return mCropMarkerMask;
    }

    public void setCropMarkerMask(final int cropMarkerMask) {
        mCropMarkerMask = cropMarkerMask;
        invalidate();
    }

    public final RectF getCropBounds() {
        RectF cropBounds = null;

        if (mCropMarkerEnabled) {
            final float width = getWidth();
            final float height = getHeight();

            if (Float.compare(width, 0F) > 0 && Float.compare(height, 0F) > 0) {
                cropBounds = new RectF(
                        mCropMarkerLeft.value / width,
                        mCropMarkerTop.value / height,
                        mCropMarkerRight.value / width,
                        mCropMarkerBottom.value / height
                );
            }
        }

        return cropBounds;
    }

    private final void resetCropMarker() {
        mCropMarkerTopLeftHandle.grab(mCropMarkerTopLeftHandle.anchor.x.value, mCropMarkerTopLeftHandle.anchor.y.value);
        mCropMarkerTopLeftHandle.move(mViewLeft.value, mViewTop.value);
        mCropMarkerTopLeftHandle.release();

        mCropMarkerBottomRightHandle.grab(mCropMarkerBottomRightHandle.anchor.x.value, mCropMarkerBottomRightHandle.anchor.y.value);
        mCropMarkerBottomRightHandle.move(mViewRight.value, mViewBottom.value);
        mCropMarkerBottomRightHandle.release();

        invalidate();
    }

    private static final class MInt {

        int value;

        MInt() { this.value = Integer.MIN_VALUE; }
        MInt(int value) { this.value = value; }

    }

    private static final class MFloat {

        float value;

        MFloat() { this.value = Float.NaN; }
        MFloat(final float value) { this.value = value; }

    }

    private static final class Point {

        @NonNull final MInt x;
        @NonNull final MInt y;

        Point() {
            this.x = new MInt();
            this.y = new MInt();
        }

        Point(@NonNull final MInt x, final MInt y) {
            this.x = x;
            this.y = y;
        }

        void set(final int x, final int y) {
            this.x.value = x;
            this.y.value = y;
        }

    }

    private static abstract class Bounds {

        abstract int left();
        abstract int top();
        abstract int right();
        abstract int bottom();

        final boolean contains(final int x, final int y) {
            return left() <= x && x <= right() && top() <= y && y <= bottom();
        }

        final boolean contains(final float x, final float y) {
            return Float.compare(left(), x) <= 0 &&
                    Float.compare(x, right()) <= 0 &&
                    Float.compare(top(), y) <= 0 &&
                    Float.compare(y, bottom()) <= 0;
        }

    }

    private static final class RectBounds extends Bounds {

        @NonNull private final MInt left;
        @NonNull private final MInt top;
        @NonNull private final MInt right;
        @NonNull private final MInt bottom;

        RectBounds() {
            this.left = new MInt();
            this.top = new MInt();
            this.right = new MInt();
            this.bottom = new MInt();
        }

        RectBounds(@NonNull final MInt left, @NonNull final MInt top, @NonNull final MInt right, @NonNull final MInt bottom) {
            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
        }

        @Override public final int left() { return left.value; }
        @Override public final int top() { return top.value; }
        @Override public final int right() { return right.value; }
        @Override public final int bottom() { return bottom.value; }

        final void set(final int left, final int top, final int right, final int bottom) {
            this.left.value = left;
            this.top.value = top;
            this.right.value = right;
            this.bottom.value = bottom;
        }

        final void add(final int x, final int y) {
            this.left.value = Math.min(this.left.value, x);
            this.top.value = Math.min(this.top.value, y);
            this.right.value = Math.max(this.right.value, x);
            this.bottom.value = Math.max(this.bottom.value, y);
        }

    }

    private static final class AnchorBounds extends Bounds {

        @NonNull private final Point anchor;
        @NonNull private final MInt threshold;

        AnchorBounds(@NonNull final Point anchor, @NonNull final MInt threshold) {
            this.anchor = anchor;
            this.threshold = threshold;
        }

        @Override public final int left() { return anchor.x.value - threshold.value; }
        @Override public final int top() { return anchor.y.value - threshold.value; }
        @Override public final int right() { return anchor.x.value + threshold.value; }
        @Override public final int bottom() { return anchor.y.value + threshold.value; }

    }

    private static abstract class TouchHandle {

        @NonNull final Point anchor;
        @NonNull final Bounds touchArea;
        @NonNull final Bounds dragBounds;
        @NonNull final Point touchPoint;

        TouchHandle(@NonNull final Point anchor, @NonNull final Bounds touchArea, @NonNull final Bounds dragBounds) {
            this.anchor = anchor;
            this.touchArea = touchArea;
            this.dragBounds = dragBounds;
            this.touchPoint = new Point(new MInt(0), new MInt(0));
        }

        double grabMatch(final int x, final int y) {
            if (touchArea.contains(x, y)) {
                return Math.sqrt(Math.pow(anchor.x.value - x, 2) + Math.pow(anchor.y.value - y, 2));
            }

            return Double.NaN;
        }

        boolean grab(final int x, final int y) {
            if (touchArea.contains(x, y)) {
                touchPoint.x.value = x - anchor.x.value;
                touchPoint.y.value = y - anchor.y.value;
                return true;
            }

            return false;
        }

        void move(final int x, final int y) {
            moveHandle(
                    Math.min(Math.max(dragBounds.left(), x - touchPoint.x.value), dragBounds.right()),
                    Math.min(Math.max(dragBounds.top(), y - touchPoint.y.value), dragBounds.bottom()));
        }

        abstract void moveHandle(int x, int y);

        void release() {
            touchPoint.x.value = 0;
            touchPoint.y.value = 0;
        }

    }

    private static final class PointTouchHandle extends TouchHandle {

        @Nullable private final MFloat mAspectRatio;
        @Nullable private final Point mAspectRatioAnchor;

        @Nullable private final RectBounds mAspectRatioBounds;

        private float mDragLineSlope;
        private float mDragLineYIntercept;

        PointTouchHandle(@NonNull final Point point, @NonNull final MInt touchThreshold, @NonNull final Bounds dragBounds, @Nullable final MFloat aspectRatio, @Nullable final Point aspectRatioAnchor) {
            super(point, new AnchorBounds(point, touchThreshold), dragBounds);

            this.mAspectRatio = aspectRatio;
            this.mAspectRatioAnchor = aspectRatioAnchor;
            this.mAspectRatioBounds = mAspectRatio != null ? new RectBounds() : null;

            this.mDragLineSlope = Float.NaN;
            this.mDragLineYIntercept = Float.NaN;
        }

        @Override
        final boolean grab(int x, int y) {
            final boolean grabbed = super.grab(x, y);

            mDragLineSlope = Float.NaN;
            mDragLineYIntercept = Float.NaN;

            if (grabbed && mAspectRatio != null && !Float.isNaN(mAspectRatio.value) && Float.compare(mAspectRatio.value, 0F) > 0) {
                // In order to keep an aspect ratio, we need to compute a drag line along which we
                // are allowed to move this touch handle. This line needs to start from the aspect
                // ratio anchor point and have the appropriate slope. In order to compute the slope,
                // we consider an imaginary point on that line. The X coordinate of this imaginary
                // line point is distanced from the aspect ratio anchor point by a POSITIVE factor,
                // in the direction of this handle's position. Using this X coordinate, we can
                // compute a matching Y value which respects the aspect ratio in relation to the
                // aspect ration anchor point. There will be 2 matching Y values, but we will consider
                // the one which is in the direction of the handle. Once we have the X,Y point, we
                // can compute the drag line slope & Y intercept (line equation). We then need to
                // define the limits of the drag line and we do this by intersecting it with the
                // drag bounds rectangle. The rectangle enclosing these intersection points represents
                // the bounds of this handle where the aspect ratio can be respected.

                // Compute the direction of this handle in relation to the aspect ratio anchor
                final int handleXDirection = anchor.x.value - mAspectRatioAnchor.x.value < 0 ? -1 : 1;
                final int handleYDirection = anchor.y.value - mAspectRatioAnchor.y.value < 0 ? -1 : 1;

                // Define a POSITIVE X axis distance from the aspect ratio anchor point
                final float factor = 100;

                // Compute the X value of the imaginary point heading towards this handle
                final PointF linePoint = new PointF();
                linePoint.x = mAspectRatioAnchor.x.value + (handleXDirection * factor);

                // Compute the two possible values for the imaginary point Y value
                final float pY1 = mAspectRatioAnchor.y.value - factor / mAspectRatio.value;
                final float pY2 = mAspectRatioAnchor.y.value + factor / mAspectRatio.value;

                // Choose the Y value which respects the direction of the handle
                if (handleYDirection == (pY1 - mAspectRatioAnchor.y.value < 0 ? -1 : 1)) {
                    linePoint.y = Math.round(pY1);
                } else if (handleYDirection == (pY2 - mAspectRatioAnchor.y.value < 0 ? -1 : 1)) {
                    linePoint.y = Math.round(pY2);
                }

                // Compute the drag line rise & run (needed to compute the slope)

                /* line_rise = y2 - y1 */
                /* line_run = x2 - x1 */
                /* line_slope = line_rise / lineRun */
                final float lineRise = mAspectRatioAnchor.y.value - linePoint.y;
                final float lineRun = mAspectRatioAnchor.x.value - linePoint.x;

                // Check if the drag line is vertical (which is not supported)
                if (Float.compare(lineRun, 0F) != 0) {
                    // Compute the slope & the Y intercept of the drag line equation

                    /* y = line_slope * x + yIntercept */

                    mDragLineSlope = lineRise / lineRun;
                    mDragLineYIntercept = linePoint.y - (mDragLineSlope * linePoint.x);

                    final int dragBoundsLeft = dragBounds.left();
                    final int dragBoundsTop = dragBounds.top();
                    final int dragBoundsRight = dragBounds.right();
                    final int dragBoundsBottom = dragBounds.bottom();

                    // Compute the intersection points of the drag line and the free drag bounds
                    final PointF[] intersectionPoints = new PointF[4];
                    intersectionPoints[0] = new PointF(dragBoundsLeft, mDragLineSlope * dragBoundsLeft + mDragLineYIntercept);
                    intersectionPoints[1] = new PointF((dragBoundsTop - mDragLineYIntercept) / mDragLineSlope, dragBoundsTop);
                    intersectionPoints[2] = new PointF(dragBoundsRight, mDragLineSlope * dragBoundsRight + mDragLineYIntercept);
                    intersectionPoints[3] = new PointF((dragBoundsBottom - mDragLineYIntercept) / mDragLineSlope, dragBoundsBottom);

                    mAspectRatioBounds.set(anchor.x.value, anchor.y.value, anchor.x.value, anchor.y.value);

                    // Define the more restricted bounds where the aspect ratio is respected
                    for (PointF intersectionPoint : intersectionPoints) {
                        if (dragBounds.contains(intersectionPoint.x, intersectionPoint.y)) {
                            mAspectRatioBounds.add(Math.round(intersectionPoint.x), Math.round(intersectionPoint.y));
                        }
                    }
                }
            }

            return grabbed;
        }

        @Override
        final void moveHandle(final int x, final int y) {
            float transformedX = x;
            float transformedY = y;

            if (!Float.isNaN(mDragLineSlope)) {
                // In order to compute a point on the drag line corresponding to the touch point,
                // we compute a perpendicular projection line from the touch point to the drag line.

                // perpendicular_slope = -1 / line_slope
                final float perpendicularLineSlope = -1 / mDragLineSlope;
                final float perpendicularLineYIntercept = transformedY - (perpendicularLineSlope * transformedX);

                // Compute the projected point
                transformedX = (perpendicularLineYIntercept - mDragLineYIntercept) / (mDragLineSlope - perpendicularLineSlope);
                transformedY = mDragLineSlope * transformedX + mDragLineYIntercept;

                // Limit the projection point to the scaling bounds
                transformedX = Math.min(Math.max(mAspectRatioBounds.left(), transformedX), mAspectRatioBounds.right());
                transformedY = Math.min(Math.max(mAspectRatioBounds.top(), transformedY), mAspectRatioBounds.bottom());
            }

            anchor.x.value = Math.round(transformedX);
            anchor.y.value = Math.round(transformedY);
        }

        @Override
        final void release() {
            super.release();

            mDragLineSlope = Float.NaN;
            mDragLineYIntercept = Float.NaN;
        }
    }

    private static final class AreaTouchHandle extends TouchHandle {

        @NonNull final MInt left;
        @NonNull final MInt top;
        @NonNull final MInt right;
        @NonNull final MInt bottom;

        AreaTouchHandle(@NonNull final MInt left, @NonNull final MInt top, @NonNull final MInt right, @NonNull final MInt bottom, @NonNull final Bounds dragBounds) {
            super(new Point(left, top), new RectBounds(left, top, right, bottom), dragBounds);

            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
        }

        @Override
        final double grabMatch(final int x, final int y) {
            if (touchArea.contains(x, y)) {
                return Math.sqrt(Math.pow(((left.value + right.value) / 2) - x, 2) + Math.pow(((top.value + bottom.value) / 2) - y, 2));
            }

            return Double.NaN;
        }

        @Override
        final void moveHandle(final int x, final int y) {
            final int deltaX = x - left.value;
            final int deltaY = y - top.value;

            left.value = x;
            top.value = y;
            right.value += deltaX;
            bottom.value += deltaY;
        }

    }

}