package com.codevblocks.core.android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;

import com.codevblocks.core.android.R;
import com.codevblocks.core.android.util.CompatUtils;
import com.codevblocks.core.android.util.ViewUtils;

public class CDBAutoCompleteTextView extends AppCompatAutoCompleteTextView implements CDBWidget {

    private static final int DEFAULT_ERROR_TEXT_VIEW_MODE = ERROR_TEXT_VIEW_MODE_ALWAYS;

    @IdRes
    private int mErrorTextViewResId;
    private int mErrorTextViewMode;
    private TextView mErrorTextView;

    private CharSequence mError;

    public CDBAutoCompleteTextView(final Context context) {
        this(context, null, 0);
    }

    public CDBAutoCompleteTextView(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CDBAutoCompleteTextView(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initialize(context, attrs);
    }

    private final void initialize(final Context context, final AttributeSet attrs) {
        this.mErrorTextViewResId = CompatUtils.Resources.ID_NULL;
        this.mErrorTextViewMode = DEFAULT_ERROR_TEXT_VIEW_MODE;
        this.mErrorTextView = null;

        final TypedArray typedArray = attrs != null ? context.getTheme().obtainStyledAttributes(attrs, R.styleable.CDBAutoCompleteTextView, 0, 0) : null;

        if (typedArray != null) {
            try {
                final int errorTextViewResId = typedArray.getResourceId(R.styleable.CDBAutoCompleteTextView_errorTextView, 0);
                if (errorTextViewResId != 0) {
                    final String resourceType = context.getResources().getResourceTypeName(errorTextViewResId);
                    if ("id".equals(resourceType)) {
                        mErrorTextViewResId = errorTextViewResId;
                    }
                }

                mErrorTextViewMode = typedArray.getInteger(R.styleable.CDBAutoCompleteTextView_errorTextViewMode, mErrorTextViewMode);
            } finally {
                typedArray.recycle();
            }
        }
    }

    @Override
    protected int[] onCreateDrawableState(final int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);

        if (mError != null) {
            mergeDrawableStates(drawableState, STATE_ERROR);
        }

        return drawableState;
    }

    @Override
    protected void onFocusChanged(final boolean focused, final int direction, final Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);

        if (mErrorTextViewMode == ERROR_TEXT_VIEW_MODE_FOCUSED && mError != null && mErrorTextView != null) {
            mErrorTextView.setText(focused ? mError : null);
        }
    }

    @Override
    public void setError(final CharSequence error, final Drawable icon) {
        this.mError = error != null && error.length() > 0 ? error : null;

        if (mErrorTextViewResId == 0) {
            super.setError(error, icon);
        } else {
            if (mErrorTextView == null || mErrorTextView.getId() != mErrorTextViewResId) {
                final View topLevelParentView = ViewUtils.getTopLevelParent(this);
                mErrorTextView = topLevelParentView.findViewById(mErrorTextViewResId);
            }

            if (mErrorTextViewMode == ERROR_TEXT_VIEW_MODE_FOCUSED) {
                if (isFocused()) {
                    mErrorTextView.setText(mError);
                }
            } else {
                mErrorTextView.setText(mError);
            }

        }

        refreshDrawableState();
    }

}
