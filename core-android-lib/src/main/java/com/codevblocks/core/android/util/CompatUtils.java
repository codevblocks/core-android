package com.codevblocks.core.android.util;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Spanned;

import androidx.annotation.AnyRes;

@SuppressWarnings("deprecation")
public final class CompatUtils {

    public static final class Html {

        public static final Spanned fromHtml(final String source) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                return android.text.Html.fromHtml(source);
            } else {
                return android.text.Html.fromHtml(source, android.text.Html.FROM_HTML_MODE_LEGACY);
            }
        }

        public static final Spanned fromHtml(final String source, final android.text.Html.ImageGetter imageGetter, android.text.Html.TagHandler tagHandler) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                return android.text.Html.fromHtml(source, imageGetter, tagHandler);
            } else {
                return android.text.Html.fromHtml(source, android.text.Html.FROM_HTML_MODE_LEGACY, imageGetter, tagHandler);
            }
        }

    }

    public static final class Resources {

        public static final @AnyRes int ID_NULL = Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q ? android.content.res.Resources.ID_NULL : 0;

        public static final int getColor(final android.content.res.Resources resources, final int id) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                return resources.getColor(id);
            } else {
                return resources.getColor(id, null);
            }
        }

        public static final Drawable getDrawable(final android.content.res.Resources resources, final int id) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                return resources.getDrawable(id);
            } else {
                return resources.getDrawable(id, null);
            }
        }

    }

    public static final class View {

        public static final void setBackground(final android.view.View view, final Drawable background) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                view.setBackgroundDrawable(background);
            } else {
                view.setBackground(background);
            }
        }

    }

}