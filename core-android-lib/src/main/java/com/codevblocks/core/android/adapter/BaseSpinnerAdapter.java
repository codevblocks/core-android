package com.codevblocks.core.android.adapter;

import android.content.Context;
import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.SpinnerAdapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

@SuppressWarnings("WeakerAccess")
public abstract class BaseSpinnerAdapter<T> implements SpinnerAdapter, ObservableAdapter {

    private final Context mContext;
    private final LayoutInflater mLayoutInflater;

    private final List<T> mData;
    private final DataSetObservable mDataSetObservable;

    private final boolean mRecycleViews;
    private final Map<Integer, View> mItemViews;
    private final Map<Integer, View> mItemDropDownViews;

    public BaseSpinnerAdapter(@NonNull final Context context, @Nullable final List<T> data) {
        this(context, data, true);
    }

    public BaseSpinnerAdapter(@NonNull final Context context, @Nullable final List<T> data, final boolean recycleViews) {
        this.mContext = context;
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.mRecycleViews = recycleViews;
        this.mItemViews = mRecycleViews ? null : new HashMap<Integer, View>();
        this.mItemDropDownViews = mRecycleViews ? null : new HashMap<Integer, View>();

        this.mDataSetObservable = new DataSetObservable();
        this.mData = data;
    }

    protected final Context getContext() {
        return mContext;
    }

    protected final LayoutInflater getLayoutInflater() {
        return mLayoutInflater;
    }

    public final List<T> getData() {
        return mData;
    }

    @Override
    public boolean isEmpty() {
        return getCount() == 0;
    }

    @Override
    public int getCount() {
        return mData != null ? mData.size() : 0;
    }

    @Override
    public T getItem(final int position) {
        return mData.get(position);
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

    @Override
    public final int getViewTypeCount() {
        return 1; // As of LOLLIPOP, view type is limited to 1
    }

    @Override
    public final int getItemViewType(final int position) {
        return AdapterView.ITEM_VIEW_TYPE_IGNORE; // As of LOLLIPOP, view type is limited to 1
    }

    public final View getView(final int position, View convertView, final ViewGroup parent) {
        if (mRecycleViews) {
            return getItemView(position, convertView, parent);
        }

        convertView = mItemViews.get(position);
        convertView = getItemView(position, convertView, parent);
        mItemViews.put(position, convertView);

        return convertView;
    }

    protected abstract View getItemView(final int position, View convertView, final ViewGroup parent);

    public final View getDropDownView(final int position, View convertView, final ViewGroup parent) {
        if (mRecycleViews) {
            getItemDropDownView(position, convertView, parent);
        }

        convertView = mItemDropDownViews.get(position);
        convertView = getItemDropDownView(position, convertView, parent);
        mItemDropDownViews.put(position, convertView);

        return convertView;
    }

    protected abstract View getItemDropDownView(int position, View convertView, ViewGroup parent);

    @Override
    public void registerDataSetObserver(final DataSetObserver observer) {
        mDataSetObservable.registerObserver(observer);
    }

    @Override
    public void unregisterDataSetObserver(final DataSetObserver observer) {
        mDataSetObservable.unregisterObserver(observer);
    }
    @Override
    public void notifyDataSetChanged() {
        mDataSetObservable.notifyChanged();
    }

    @Override
    public void notifyDataSetInvalidated() {
        mDataSetObservable.notifyInvalidated();
    }

}
