package com.codevblocks.core.android.widget;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatSpinner;

public class CDBSpinner extends AppCompatSpinner implements CDBWidget {

    public CDBSpinner(final Context context) {
        super(context);
    }

    public CDBSpinner(final Context context, final int mode) {
        super(context, mode);
    }

    public CDBSpinner(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public CDBSpinner(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CDBSpinner(final Context context, final AttributeSet attrs, final int defStyleAttr, final int mode) {
        super(context, attrs, defStyleAttr, mode);
    }

    public CDBSpinner(final Context context, final AttributeSet attrs, final int defStyleAttr, final int mode, final Resources.Theme popupTheme) {
        super(context, attrs, defStyleAttr, mode, popupTheme);
    }

}
